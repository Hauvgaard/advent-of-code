use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut valid_lines_remain = vec![];
    let mut stack = vec![];
    let mut error_points = 0;
    for line in input_data {
        let mut corrupt = false;
        for c in line.chars() {
            match c {
                '{' | '(' | '[' | '<' => stack.push(c),
                '}' | ')' | ']' | '>' => {
                    if brace_match(stack.pop().unwrap()) != c {
                        corrupt = true;
                        error_points += match c {
                            ')' => 3,
                            ']' => 57,
                            '}' => 1197,
                            '>' => 25137,
                            _ => unreachable!(),
                        };
                        break;
                    }
                }
                _ => unreachable!(),
            }
        }
        if !corrupt {
            valid_lines_remain.push(stack.clone());
        }
        stack.clear();
    }

    println!("Error points: {}", error_points);

    /////////////////
    //// Solve 2 ////
    /////////////////

    let mut comp_points = vec![];
    for mut remains in valid_lines_remain {
        let mut score = 0u64;
        while let Some(brace) = remains.pop() {
            score *= 5;
            score += match brace_match(brace) {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                '>' => 4,
                _ => unreachable!(),
            }
        }
        comp_points.push(score);
    }
    comp_points.sort_unstable();
    println!("Middle score: {}", comp_points[comp_points.len() / 2]);

    Ok(())
}

fn brace_match(brace: char) -> char {
    match brace {
        '(' => ')',
        '{' => '}',
        '[' => ']',
        '<' => '>',
        _ => unreachable!(),
    }
}
