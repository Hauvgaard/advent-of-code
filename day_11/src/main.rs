use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let mut input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| {
            x.unwrap()
                .chars()
                .map(|x| x.to_digit(10).unwrap() as i32)
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<Vec<i32>>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut flash_count = 0;
    for _ in 0..100 {
        input_data = input_data
            .into_iter()
            .map(|y| y.into_iter().map(|x| x + 1).collect())
            .collect();
        for y in 0..input_data.len() {
            for x in 0..input_data[0].len() {
                if input_data[y][x] > 9 {
                    flash_count += inc_point(&mut input_data, y, x);
                }
            }
        }
    }

    println!("Flash count: {}", flash_count);

    /////////////////
    //// Solve 2 ////
    /////////////////

    // Assume sync after iteration 100

    let mut iteration = 100;
    while input_data
        .iter()
        .map(|x| x.iter().sum::<i32>())
        .sum::<i32>()
        > 0
    {
        iteration += 1;
        input_data = input_data
            .into_iter()
            .map(|y| y.into_iter().map(|x| x + 1).collect())
            .collect();
        for y in 0..input_data.len() {
            for x in 0..input_data[0].len() {
                if input_data[y][x] > 9 {
                    inc_point(&mut input_data, y, x);
                }
            }
        }
    }

    println!("Sync iteration: {}", iteration);
    Ok(())
}

fn inc_point(map: &mut Vec<Vec<i32>>, y: usize, x: usize) -> u32 {
    if y >= map.len() || x >= map[0].len() || map[y][x] == 0 {
        return 0;
    }
    map[y][x] += 1;

    let mut flashes = 0;

    if map[y][x] > 9 {
        map[y][x] = 0;
        flashes += 1;
        flashes += inc_point(map, y - 1, x - 1);
        flashes += inc_point(map, y - 1, x);
        flashes += inc_point(map, y - 1, x + 1);
        flashes += inc_point(map, y, x - 1);
        flashes += inc_point(map, y, x + 1);
        flashes += inc_point(map, y + 1, x - 1);
        flashes += inc_point(map, y + 1, x);
        flashes += inc_point(map, y + 1, x + 1);
    }
    flashes
}
