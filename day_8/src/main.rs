use std::fs::File;
use std::io::{self, BufRead};

macro_rules! add_digit {
    ($map:ident, $from:ident, $filter:block, $digit:expr) => {
        $map.push((&$from.clone().find($filter).unwrap(), $digit))
    };
}

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut outputs = input_data
        .iter()
        .map(|x| x.split(" | ").nth(1).unwrap())
        .collect::<Vec<&str>>();

    let mut uniq_sums = 0;
    for out in outputs {
        for digit in out.split_ascii_whitespace() {
            uniq_sums += match digit.len() {
                2 | 3 | 4 | 7 => 1,
                _ => 0,
            }
        }
    }

    println!("Uniq Sum: {}", uniq_sums);

    /////////////////
    //// Solve 2 ////
    /////////////////

    outputs = input_data
        .iter()
        .map(|x| x.split(" | ").nth(1).unwrap())
        .collect::<Vec<&str>>();

    let inputs = input_data.iter().map(|x| x.split(" | ").next().unwrap());

    let mut sum = 0;
    for (i, line) in inputs.enumerate() {
        let mut map: Vec<(&str, &str)> = vec![];
        let digits = line.split(' ');
        let d1 = digits.clone().find(|x| x.len() == 2).unwrap();
        map.push((d1, "1"));
        let d4 = digits.clone().find(|x| x.len() == 4).unwrap();
        map.push((d4, "4"));
        add_digit!(map, digits, { |x| x.len() == 3 }, "7");
        add_digit!(map, digits, { |x| x.len() == 7 }, "8");
        let l4 = &string_xor(d1, d4);
        let l5_vals = digits.clone().filter(|x| x.len() == 5);
        add_digit!(map, l5_vals, { |x| lax_contains(x, l4) }, "5");
        add_digit!(map, l5_vals, { |x| lax_contains(x, d1) }, "3");
        add_digit!(map, l5_vals, { |x| !map_contains(&map, x) }, "2");
        let l6_vals = digits.filter(|x| x.len() == 6);
        add_digit!(map, l6_vals, { |x| !lax_contains(x, l4) }, "0");
        add_digit!(
            map,
            l6_vals,
            { |x| lax_contains(x, d1) && lax_contains(x, l4) },
            "9"
        );
        add_digit!(map, l6_vals, { |x| !map_contains(&map, x) }, "6");

        let mut str_out = String::new();
        for s in outputs[i].split_ascii_whitespace() {
            str_out += &map_get(&map, s);
        }
        let number = str_out.parse::<u32>().unwrap();
        sum += number;
    }
    println!("Sum: {}", sum);

    Ok(())
}

fn map_get(map: &[(&str, &str)], key: &str) -> String {
    for (k, v) in map {
        if k.len() == key.len() && lax_contains(k, key) {
            return v.to_string();
        }
    }
    unreachable!();
}

fn map_contains(map: &[(&str, &str)], key: &str) -> bool {
    for (k, _) in map {
        if k.len() == key.len() && lax_contains(k, key) {
            return true;
        }
    }
    false
}

fn lax_contains(s: &str, pat: &str) -> bool {
    for c in pat.chars() {
        if !s.contains(c) {
            return false;
        }
    }
    true
}

fn string_xor(s1: &str, s2: &str) -> String {
    let mut output = String::new();
    let mut s2c = s2.to_string();
    for c in s1.chars() {
        if !s2.contains(c) {
            output += &String::from(c);
        } else {
            s2c = s2c.replace(c, "");
        }
    }
    output += &s2c;
    output
}
