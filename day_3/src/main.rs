use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut bit_freq = vec![0i32; 12];
    for line in &input_data {
        for (i, chr) in line.char_indices() {
            match chr {
                '1' => bit_freq[i] += 1,
                '0' => bit_freq[i] -= 1,
                _ => unreachable!(),
            }
        }
    }

    let mut gamma = 0;
    for (i, bit) in bit_freq.iter().enumerate() {
        if *bit > 0 {
            gamma |= 1 << (11 - i)
        }
    }
    // Epsilon is just a bit flipped gamma with the
    // same number of bits
    let epsilon = !gamma & 0b111111111111;
    println!("Power usage: {}", gamma * epsilon);

    /////////////////
    //// Solve 2 ////
    /////////////////

    let oxygen = find_value(&input_data, |avg: i32| avg >= 0);
    let co2 = find_value(&input_data, |avg: i32| avg < 0);

    println!("Life Support rating: {}", oxygen * co2);

    Ok(())
}

fn find_value(list: &[String], pred: fn(i32) -> bool) -> i32 {
    let mut i = 0;
    let mut nlist = list.iter().collect::<Vec<&String>>();
    while nlist.len() > 1 {
        let should_be_1 = pred(
            nlist
                .iter()
                .map(|x| {
                    if x.chars().nth(i).unwrap() == '1' {
                        1
                    } else {
                        -1
                    }
                })
                .sum::<i32>(),
        );

        nlist = nlist
            .into_iter()
            .filter(|x| (x.chars().nth(i).unwrap() == '1') == should_be_1)
            .collect();
        i += 1;
    }
    i32::from_str_radix(nlist[0], 2).unwrap()
}
