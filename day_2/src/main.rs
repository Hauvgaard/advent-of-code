use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut real_depth = 0;
    let mut depth = 0;
    let mut horizontal = 0;

    for command in input_data {
        let mut command_parts = command.split_ascii_whitespace();
        let dir: &str = command_parts.next().unwrap();
        let length: u32 = command_parts.next().unwrap().parse::<u32>().unwrap();

        match dir {
            "forward" => {
                horizontal += length;
                // As problem 1's depth actually is the aim value
                // depth is reused as the aim
                real_depth += depth * length;
            }
            "up" => depth -= length,
            "down" => depth += length,
            _ => unreachable!(),
        }
    }

    println!("depth/horizontal product: {}", depth * horizontal);

    /////////////////
    //// Solve 2 ////
    /////////////////

    println!("real_depth/horizontal product: {}", real_depth * horizontal);
    Ok(())
}
