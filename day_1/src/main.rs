use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| x.unwrap().parse::<i32>().unwrap())
        .collect::<Vec<i32>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut n_larger = 0;

    // Count number of inputs, larger than the previous
    for i in 1..input_data.len() {
        if input_data[i] > input_data[i - 1] {
            n_larger += 1;
        }
    }

    println!("n_larger: {}", n_larger);

    /////////////////
    //// Solve 2 ////
    /////////////////

    let mut n_group_larger = 0;

    // As any adjacent group will share the middle 2 elements, these can be ignored
    // The problem can then be simplified to checking any number with the number
    // 3 places behind
    for i in 3..input_data.len() {
        if input_data[i] > input_data[i - 3] {
            n_group_larger += 1;
        }
    }
    println!("n_group_larger: {}", n_group_larger);
    Ok(())
}
