use std::fs::File;
use std::io::{self, BufRead};

#[derive(Debug, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
}

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| {
            x.unwrap()
                .split("->")
                .map(|x| Point::parse(x))
                .collect::<Vec<Point>>()
        })
        .collect::<Vec<Vec<Point>>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    // Get non-diagonal lines
    let cross_lines = input_data
        .clone()
        .into_iter()
        .filter(|x| x[0].x == x[1].x || x[0].y == x[1].y)
        .collect::<Vec<Vec<Point>>>();

    let mut map = [[0u8; 1000]; 1000];

    write_lines(&mut map, cross_lines);

    println!("Sum: {}", sum_intersections(&map));

    /////////////////
    //// Solve 2 ////
    /////////////////

    let diag_lines = input_data
        .into_iter()
        .filter(|x| x[0].x != x[1].x && x[0].y != x[1].y)
        .collect::<Vec<Vec<Point>>>();

    write_lines(&mut map, diag_lines);

    println!("Sum: {}", sum_intersections(&map));

    Ok(())
}

fn sum_intersections(map: &[[u8; 1000]; 1000]) -> u32 {
    let mut sum = 0;
    for x in 0..1000 {
        for y in 0..1000 {
            if map[x][y] > 1 {
                sum += 1;
            }
        }
    }
    sum
}

fn write_lines(map: &mut [[u8; 1000]; 1000], lines: Vec<Vec<Point>>) {
    for line in lines {
        let dx = line[1].x - line[0].x;
        let dy = line[1].y - line[0].y;

        let sdx = dx.signum();
        let sdy = dy.signum();

        for i in 0..=dx.abs().max(dy.abs()) {
            map[(line[0].y + i * sdy) as usize][(line[0].x + i * sdx) as usize] += 1;
        }
    }
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
        Point { x, y }
    }
    fn parse(s: &str) -> Self {
        let mut spl = s.split(',');
        let x = spl.next().unwrap().trim().parse::<i32>().unwrap();
        let y = spl.next().unwrap().trim().parse::<i32>().unwrap();

        Self::new(x, y)
    }
}
