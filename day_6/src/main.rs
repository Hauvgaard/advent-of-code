use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .nth(0)
        .unwrap()
        .unwrap()
        .split(',')
        .map(|x| x.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let mut fish = frequency(input_data);
    for _ in 0..80 {
        run_day(&mut fish);
    }

    let sum_80: usize = fish.iter().sum();
    println!("N_fish: {}", sum_80);

    /////////////////
    //// Solve 2 ////
    /////////////////

    for _ in 80..256 {
        run_day(&mut fish);
    }
    let sum_256: usize = fish.iter().sum();
    println!("N_fish: {}", sum_256);

    Ok(())
}

fn frequency(vec: Vec<usize>) -> Vec<usize> {
    let mut freq = vec![0, 0, 0, 0, 0, 0, 0, 0, 0];
    for v in vec {
        freq[v] += 1;
    }
    freq
}

fn run_day(fish: &mut Vec<usize>) {
    let zero_fish = fish[0];
    for i in 0..=8 {
        fish[i] = match i {
            0..=5 | 7 => fish[i + 1],
            6 => zero_fish + fish[7],
            8 => zero_fish,
            _ => unreachable!(),
        }
    }
}
