use std::fs::File;
use std::io::{self, BufRead};

#[derive(Debug, Clone)]
struct BingoBoard {
    nums: Vec<(u8, bool)>,
}

fn main() -> io::Result<()> {
    let mut input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| x.unwrap());

    /////////////////
    //// Solve 1 ////
    /////////////////

    let num_line = input_data.next().unwrap();
    let mut drawn_nums = num_line.split(',').map(|x| x.parse::<u8>().unwrap());

    let mut boards: Vec<BingoBoard> = vec![];

    // Consume empty lines between boards
    while let Some(_) = input_data.next() {
        let mut board = vec![];
        for _ in 0..5 {
            board.append(
                &mut input_data
                    .next()
                    .unwrap()
                    .split_ascii_whitespace()
                    .map(|x| x.parse::<u8>().unwrap())
                    .collect::<Vec<u8>>(),
            );
        }
        boards.push(BingoBoard::new(board));
    }

    let mut draw;
    'outer: loop {
        draw = drawn_nums.next().unwrap();
        for board in &mut boards {
            board.set_number(draw);
            if board.has_won() {
                let score = board.calc_score();
                println!("Winning board score: {}", score * draw as u32);
                break 'outer;
            }
        }
    }

    /////////////////
    //// Solve 2 ////
    /////////////////

    loop {
        for board in &mut boards {
            board.set_number(draw);
        }
        if boards.len() != 1 {
            boards = boards.into_iter().filter(|x| !x.has_won()).collect();
        } else if boards[0].has_won() {
            let score = boards[0].calc_score();
            println!("Losing board score: {}", score * draw as u32);
            break;
        }

        // Draw new number at the end, to ensure winning number from
        // solution 1 is applied to all boards
        draw = drawn_nums.next().unwrap();
    }

    Ok(())
}

impl BingoBoard {
    fn new(nums: Vec<u8>) -> BingoBoard {
        BingoBoard {
            nums: nums.into_iter().map(|x| (x, false)).collect(),
        }
    }
    fn set_number(&mut self, num: u8) {
        for i in 0..self.nums.len() {
            if self.nums[i].0 == num {
                self.nums[i].1 = true;
                return;
            }
        }
    }

    fn has_won(&self) -> bool {
        // Check for horizontal win
        'rows_horizontal: for i in 0..5 {
            for j in 0..5 {
                if !self.nums[i * 5 + j].1 {
                    continue 'rows_horizontal;
                }
            }
            return true;
        }
        // Check for vertical win
        'rows_vertical: for i in 0..5 {
            for j in 0..5 {
                if !self.nums[i + j * 5].1 {
                    continue 'rows_vertical;
                }
            }
            return true;
        }
        false
    }
    fn calc_score(&self) -> u32 {
        let mut sum: u32 = 0;
        for (n, checked) in &self.nums {
            if !checked {
                sum += *n as u32;
            }
        }
        sum
    }
}
