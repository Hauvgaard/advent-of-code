use std::fs::File;
use std::io::{self, BufRead};

use std::collections::VecDeque;

fn main() -> io::Result<()> {
    let input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .map(|x| {
            x.unwrap()
                .chars()
                .map(|x| x.to_digit(10).unwrap() as i32)
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<Vec<i32>>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    let height = input_data.len() as i32;
    let width = input_data[0].len() as i32;
    let around: [(i32, i32); 4] = [(-1, 0), (0, -1), (0, 1), (1, 0)];

    let mut valleys = vec![];

    for y in 0..height {
        'x_loop: for x in 0..width {
            let current = input_data[y as usize][x as usize];
            for (dy, dx) in &around {
                let nx = x + dx;
                let ny = y + dy;
                if nx < width && nx >= 0 && ny < height && ny >= 0 {
                    if input_data[ny as usize][nx as usize] <= current {
                        continue 'x_loop;
                    }
                }
            }
            valleys.push((y, x));
        }
    }

    let risk_level: i32 = valleys.len() as i32
        + valleys
            .iter()
            .map(|x| input_data[x.0 as usize][x.1 as usize])
            .sum::<i32>();
    println!("Risk level: {}", risk_level);

    /////////////////
    //// Solve 2 ////
    /////////////////

    let mut valley_sizes: VecDeque<usize> = VecDeque::new();
    for valley in valleys {
        let mut size = 0;
        let mut visited: Vec<(i32, i32)> = vec![];
        let mut queue: VecDeque<(i32, i32)> = VecDeque::new();
        queue.push_back((valley.0, valley.1));

        while let Some((py, px)) = queue.pop_front() {
            visited.push((py, px));
            size += 1;
            for (dy, dx) in &around {
                let nx = px + dx;
                let ny = py + dy;
                if nx < width && nx >= 0 && ny < height && ny >= 0 {
                    if input_data[ny as usize][nx as usize] != 9
                        && !visited.contains(&(ny, nx))
                        && !queue.contains(&(ny, nx))
                    {
                        queue.push_front((ny, nx));
                    }
                }
            }
        }
        valley_sizes.push_back(size);
    }
    valley_sizes.make_contiguous().sort();

    let mut product = 1;
    for _ in 0..3 {
        product *= valley_sizes.pop_back().unwrap();
    }
    println!("Largests valley size product: {}", product);

    Ok(())
}
