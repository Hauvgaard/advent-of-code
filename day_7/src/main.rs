use std::fs::File;
use std::io::{self, BufRead};

fn main() -> io::Result<()> {
    let mut input_data = io::BufReader::new(File::open("./input")?)
        .lines()
        .nth(0)
        .unwrap()
        .unwrap()
        .split(',')
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();

    /////////////////
    //// Solve 1 ////
    /////////////////

    // Find median value
    input_data.sort_unstable();

    let median = input_data[input_data.len() / 2];

    let mut fuel = 0;
    for i in &input_data {
        fuel += (i - median).abs();
    }
    println!("Fuel needed: {}", fuel);

    /////////////////
    //// Solve 2 ////
    /////////////////

    // Find average value
    let avg: f32 = (input_data.iter().sum::<i32>() as f32 / input_data.len() as f32);
    // Round decimal points below 0.9 down and the rest up.
    // Found through testing
    let best_pos = match avg.fract() {
        x if x < 0.9 => avg.floor() as i32,
        _ => avg.ceil() as i32,
    };

    fuel = 0;
    for i in input_data {
        let diff = (i - best_pos).abs();
        fuel += ((diff * (diff + 1)) / 2);
    }
    println!("Real fuel needed: {}", fuel);

    Ok(())
}
